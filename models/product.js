const mongoose = require ("mongoose");

const productSchema = new mongoose.Schema({
name: {
		type: String,
		required: [true, "Product is required"]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	bookedOn: {
		type: Date,
		default: new Date()
	},

	customers: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			bookedOn: {
				type: Date,
				default: new Date()
			}
		}


	]
})

module.exports = mongoose.model("Product", productSchema);