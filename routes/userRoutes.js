const express = require("express");

const router = express.Router();

const auth = require("../auth");

const UserControllers = require('../controllers/userControllers');



router.post("/checkEmail", (req , res)=>{
	UserControllers.checkEmailExists(req.body).then(result => res.send(result))
});

router.post("/register", (req , res)=>{
	UserControllers.registerUser(req.body).then(result => res.send(result))
});


router.post("/login",(req, res) => {
	UserControllers.loginUser(req.body).then(result => res.send(result))
})

router.get("/details", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);

	UserControllers.getProfile(userData.id).then(result => res.send (result))

})

router.post("/book", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	UserControllers.book(data).then(result => res.send(result));
})


module.exports = router
