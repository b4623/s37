const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/productControllers");
const auth = require('../auth')

router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data.isAdmin)

	if(data.isAdmin) {
		ProductController.addProduct(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})


router.get("/all", (req, res) =>{
	ProductController.getAllProducts().then(result => res.send(result));
});



router.get("/", (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))
});



router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	ProductController.getProduct(req.params.productId).then(result => res.send(result))
})



router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})

router.put("/:productId/bookings", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.hedears.authorization).isAdmin
	}
	if(data.isAdmin){
		ProductController.archieveProduct(req.params.productId).then(result => res.send(result))
	}else{
		res.send(false);
	}
})

// router.put("/:productId/bookings", auth.verify, auth.verifyAdmin, (req, res) => {
// 	ProductController.archiveProduct(req.params).then(
// 		resultFromController => res.send(resultFromController))
// })







module.exports = router;