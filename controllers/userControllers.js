const User = require ("../models/user");
const bcrypt = require ("bcrypt");
const auth = require("../auth")

const Product = require("../models/product");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then( result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((User, err) => {
		if (err){
			return false;
		}else{
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email}).then(result =>{
		if(result == null) {
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

					return{accessToken: auth.createAccessToken(result.toObject())}
			}	else{
				return false;
			}

		}
	})
}
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {

		result.password = "";

		return result;
	})
}

module.exports.book = async (data)=> {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.bookings.push({productId: data.productId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			}else{
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.bookings.push({userId: data.userId});

		return product.save().then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	});

	if ( isUserUpdated && isProductUpdated){
		return true;
	}else{
		return false;
	}



}

