const Product = require("../models/product");
const Auth = require("../auth");


module.exports.addProduct = (reqBody) => {
	console.log(reqBody);

	
	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price
	});

	
	return newProduct.save().then((product, error) => {
	
		if(error) {
			return false;
		} else {
			
			return true;
		}
	})

}






module.exports.getAllProducts = () => {
	return Product.find({}).then( result => {
		return result;
	})
}



module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;
	})
}


module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result;
	})
}




module.exports.updateProduct = (productId, reqBody) => {
	//specify the properties of the doc to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		//course not updated
		if(error) {
			return false;
		}else {
			//course updated successfully
			return true;
		}
	})

}

module.exports.archieveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	}
	return Product.findByIdandUpdate(reqParams, updateActiveFields).then((product, error)=>{
		if (error){
			return false
		}
	})
}


// module.exports.archiveProduct = (params) => {
// 	let updateActiveField = {
// 		isActive: false
// 	}
// 	return Product.findByIdAndUpdate(params.productId, updateActiveField).then((product, error) => {
// 		if(error){
// 			return false;
// 		}
// 		else{
// 			return true;
// 		}
// 	});
// }


