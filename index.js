const express = require('express');

const mongoose = require("mongoose");

require('dotenv').config();

const cors = require("cors");

const userRoutes = require('./routes/userRoutes');

const productRoutes = require("./routes/productRoutes");

const app = express();

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.use("/captwo", userRoutes);

app.use("/captwo/products", productRoutes);

mongoose.connect("mongodb+srv://mikemike:mikemike@cluster0.xb2ky.mongodb.net/s37?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log('Now Connected to MongoDB Atlas'));






app.listen(process.env.PORT, () => {
	console.log(`API is now online on port:${process.env.PORT}`)
})